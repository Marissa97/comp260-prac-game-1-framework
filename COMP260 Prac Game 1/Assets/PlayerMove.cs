﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript1 : MonoBehaviour {

	public float maxSpeed = 5.0f;

	// Use this for initialization
	void Start () {
		
	}

	public Vector2 move;
	public Vector2 velocity; 

	void Update() {

		//Vector2 move = velocity * Time.deltaTime;
		// move the object
		//transform.Translate(move);

		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis("Horizontal");
		direction.y = Input.GetAxis("Vertical");

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);
	}
}
